<?php

namespace App\Components\Screening;

use App\Model\Entities\Genre;
use App\Model\Entities\Screening,
	Nette\Application\UI,
	Doctrine\ORM\EntityManager,
	App\Model\Entities\Cinema,
	App\Model\Entities\Room,
	App\Model\Entities\Movie;
use Nette\Utils\DateTime;

/**
 * Class ScreeningControl
 */
class ScreeningControl extends UI\Control
{

	/**
	 * @var callable
	 */
	public $onSuccess = [];

	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * @var int
	 */
	private $id;

	/**
	 * ScreeningControl constructor.
	 * @param int|null $id
	 * @param EntityManager $em
	 */
	public function __construct($id, EntityManager $em)
	{
		parent::__construct();
		$this->id = $id;
		$this->em = $em;
	}

	public function render()
	{
		if(!is_null($this->id))
		{
			$r_screening = $this->em->getRepository(Screening::getClassName());
			$screening = $r_screening->find($this->id);

			$this['screening']['film']->setValue($screening->movie->id);
			$this['screening']['time']->setValue($screening->time->format("Y-m-d H:m"));
			$this['screening']['room']->setDefaultValue($screening->room->id);
			$this['screening']['price']->setDefaultValue($screening->price);
		}

		$this->template->setFile(__DIR__ . '/screening.latte');
		$this->template->render();
	}

	/**
	 * @return UI\Form
	 */
	protected function createComponentScreening()
	{
		$form = new UI\Form;
		$form->addSelect('film', 'Film*:', $this->getMovies())
				->setAttribute('class', 'form-control')
				->setRequired("Povinné pole!");
		$form->addText('time', 'Čas*:')
				->setHtmlId("datetimepicker")
				->setAttribute('class', 'form-control');
		$form->addSelect('room', 'Místnost*:', $this->getRooms())->setAttribute('class', 'form-control');;
		$form->addText('price', 'Cena*:')
				->setAttribute('class', 'form-control')
				->addRule(UI\Form::INTEGER, 'Zadejte celočíslenou hodnotu!')
				->setRequired("Povinné pole!");
		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = function (UI\Form $form)
		{
			$this->process($form);
		};

		return $form;
	}

	/**
	 * @param UI\Form $form
	 */
	protected function process(UI\Form $form)
	{
		$values = $form->getValues();

		$r_movie = $this->em->getRepository(Movie::getClassName());
		$r_room = $this->em->getRepository(Room::getClassName());

		if(is_null($this->id))
		{
			$screening = new Screening();
			$this->flashMessage("Promítání bylo uloženo.");
		}
		else
		{
			$r_screening = $this->em->getRepository(Screening::getClassName());
			$screening = $r_screening->find($this->id);
			$this->flashMessage("Promítání bylo změněn.");
		}

		$screening->movie = $r_movie->find($values['film']);
		$screening->time = new DateTime($values['time']);
		$screening->room = $r_room->find($values['room']);
		$screening->price = (int)$values['price'];

		$this->em->persist($screening);
		$this->em->flush();

		$this->onSuccess();


	}

	private function getMovies()
	{
		$movies = array();
		foreach($this->em->getRepository(Movie::getClassName())->findAll() as $movie)
		{
			$movies[$movie->id] = $movie->title;
		}

		return $movies;
	}

	private function getCinemas()
	{
		$cinemas = array();
		foreach($this->em->getRepository(Cinema::getClassName())->findAll() as $cinema)
		{
			$cinemas[$cinema->id] = $cinema->name;
		}

		return $cinemas;
	}

	private function getRooms()
	{
		$rooms = array();
		foreach($this->em->getRepository(Cinema::getClassName())->findAll() as $cinema)
		{
			foreach($cinema->rooms as $room)
			{
				$rooms[$cinema->name][$room->id] = $room->name;
			}
		}

		return $rooms;
	}
}
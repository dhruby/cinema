<?php
namespace App\Model\Facades;

use App\Model\Entities\Cinema;
use Kdyby\Doctrine\EntityManager,
    Nette\Object;


/**
 * @package App\Model\Facades
 */
class CinemaFacade extends Object
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFirstRow()
    {
        $r_cinema = $this->em->getRepository(Cinema::getClassName());
        $row = $r_cinema->createQueryBuilder("c")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        return $row;
    }
}
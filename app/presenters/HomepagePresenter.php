<?php

namespace App\Presenters;

use Nette,
	App\Model,
	App\Components\Program\IProgramFactory,
	App\Components\Program\ProgramControl,
	Nette\Utils\DateTime,
	App\Model\Facades\ScreeningFacade,
	App\Model\Facades\CinemaFacade;


class HomepagePresenter extends BasePresenter
{
	/**
	 * @var ScreeningFacade
	 * @inject
	 */
	public $screeningFacade;

	/**
	 * @var CinemaFacade
	 * @inject
	 */
	public $cinemaFacade;

	private $date;

	private $cinema;

	public function actionDefault()
	{
		$this->date = new DateTime('today');
		$this->cinema = $this->cinemaFacade->getFirstRow()->id;
	}

	public function renderDefault()
	{
		$this->template->program = $this->screeningFacade->getScreeningsByDay($this->date, $this->cinema);
	}

	public function handleChangeProgram($time, $cinema)
	{
		$this->date = new DateTime($time);
		$this->cinema = $cinema;

		if ($this->isAjax()) {
			$this->redrawControl('program');
		}
	}

	/**
	 * @param IProgramFactory $programFactory
	 * @return ProgramControl
	 */
	protected function createComponentProgram(IProgramFactory $programFactory)
	{
		$program = $programFactory->create($this->getParameter('id'));
		$program->onSuccess[] = function ()
		{

		};

		return $program;
	}

}

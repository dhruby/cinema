-- Adminer 4.2.2fx MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `cinema` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `cinema`;

DROP TABLE IF EXISTS `cinema`;
CREATE TABLE `cinema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `street` varchar(50) COLLATE utf8_bin NOT NULL,
  `house_number` varchar(10) COLLATE utf8_bin NOT NULL,
  `city` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `cinema` (`id`, `name`, `street`, `house_number`, `city`) VALUES
(1,	'Kino Letná',	'Letná',	'57',	'Brno'),
(2,	'Kino Komárov',	'Komárov',	'322',	'Brno'),
(3,	'Kino Královo pole',	'Božetěchova',	'165',	'Brno'),
(4,	'Kino Žabovřesky',	'Strmá',	'457',	'Brno'),
(5,	'Kino Černá pole',	'Musilova',	'222',	'Brno');

DROP TABLE IF EXISTS `genre`;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `genre` (`id`, `name`) VALUES
(1,	'Drama'),
(2,	'Komedie'),
(3,	'Krimi'),
(4,	'Horor'),
(5,	'Thriller'),
(6,	'Historický film'),
(7,	'Pohádka'),
(8,	'Dokumentární');

DROP TABLE IF EXISTS `movie`;
CREATE TABLE `movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_bin NOT NULL,
  `length` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `movie` (`id`, `title`, `length`) VALUES
(1,	'Superman',	125),
(2,	'Nikdy nejsme sami',	105),
(12,	'Sinister 2',	134),
(13,	'Love',	87),
(14,	'Babel',	0),
(15,	'Bobule',	105),
(16,	'Colette',	100);

DROP TABLE IF EXISTS `movie_genre`;
CREATE TABLE `movie_genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `movie_id` (`movie_id`),
  KEY `genre_id` (`genre_id`),
  CONSTRAINT `movie_genre_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`),
  CONSTRAINT `movie_genre_ibfk_2` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `movie_genre` (`id`, `movie_id`, `genre_id`) VALUES
(1,	1,	2),
(2,	2,	3),
(3,	1,	3),
(5,	12,	4),
(6,	13,	1),
(7,	13,	2),
(8,	14,	3),
(9,	15,	2),
(10,	16,	6);

DROP TABLE IF EXISTS `room`;
CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cinema_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cinema_id` (`cinema_id`),
  CONSTRAINT `room_ibfk_1` FOREIGN KEY (`cinema_id`) REFERENCES `cinema` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `room` (`id`, `cinema_id`, `name`) VALUES
(1,	1,	'A201'),
(2,	1,	'A202'),
(3,	1,	'A203'),
(4,	2,	'B201'),
(5,	2,	'B202'),
(6,	2,	'B203'),
(7,	3,	'C201'),
(8,	3,	'C202'),
(9,	3,	'C203'),
(10,	4,	'D201'),
(11,	4,	'D202'),
(12,	4,	'D203'),
(13,	5,	'E201'),
(14,	5,	'E202'),
(15,	5,	'E203');

DROP TABLE IF EXISTS `screening`;
CREATE TABLE `screening` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `room_id` int(11) NOT NULL,
  `price` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `movie_id` (`movie_id`),
  CONSTRAINT `screening_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  CONSTRAINT `screening_ibfk_2` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `screening` (`id`, `movie_id`, `time`, `room_id`, `price`) VALUES
(3,	2,	'2016-04-04 15:30:00',	1,	120),
(4,	2,	'2016-04-05 13:00:00',	1,	150),
(5,	2,	'2016-04-04 15:30:00',	4,	120),
(7,	13,	'2016-04-04 15:30:00',	11,	160),
(8,	13,	'2016-04-04 15:30:00',	12,	160),
(10,	13,	'2016-04-04 15:30:00',	13,	160),
(11,	14,	'2016-04-04 15:30:00',	1,	120),
(12,	15,	'2016-04-05 13:00:00',	2,	150),
(13,	16,	'2016-04-04 18:30:00',	3,	120),
(14,	12,	'2016-04-04 17:30:00',	1,	160),
(15,	1,	'2016-04-04 11:30:00',	2,	160),
(16,	2,	'2016-04-04 19:30:00',	3,	160),
(17,	14,	'2016-04-04 20:30:00',	4,	120),
(18,	15,	'2016-04-05 21:00:00',	5,	150),
(19,	16,	'2016-04-04 15:30:00',	6,	140),
(20,	12,	'2016-04-04 16:30:00',	4,	160),
(21,	1,	'2016-04-04 14:30:00',	5,	150),
(22,	2,	'2016-04-04 13:30:00',	6,	120),
(23,	14,	'2016-04-04 11:30:00',	9,	120),
(24,	15,	'2016-04-05 15:00:00',	8,	150),
(25,	16,	'2016-04-04 16:30:00',	7,	140),
(26,	12,	'2016-04-04 17:30:00',	9,	160),
(27,	1,	'2016-04-04 21:30:00',	8,	150),
(28,	2,	'2016-04-04 20:30:00',	7,	120),
(29,	14,	'2016-04-04 11:15:00',	11,	120),
(30,	15,	'2016-04-05 14:45:00',	12,	150),
(31,	16,	'2016-04-04 21:30:00',	10,	140),
(32,	12,	'2016-04-04 11:30:00',	11,	160),
(33,	1,	'2016-04-04 19:30:00',	12,	150),
(34,	2,	'2016-04-04 20:00:00',	10,	120),
(35,	14,	'2016-04-04 15:15:00',	13,	120),
(36,	15,	'2016-04-05 11:45:00',	14,	150),
(37,	16,	'2016-04-04 20:30:00',	15,	140),
(38,	12,	'2016-04-04 21:30:00',	13,	160),
(39,	1,	'2016-04-04 14:30:00',	14,	150),
(40,	2,	'2016-04-04 20:15:00',	15,	120);

-- 2016-04-05 20:24:10